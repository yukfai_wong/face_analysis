# README #

An jupyter notebook that 
1. Blur the background from the face

2. Identify profiles where the face is at least 50-60% of the overall photo. If it�s less than 50%, then score of face quality being too small

3. Identify if teeth are visible. If so, mention teeth is shown

4. Identity sentiment on face. E.g. smiling / happy, or neutral, sad.