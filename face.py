import json
import os
import sys
import time
import requests
import numpy as np
import seaborn as sns
import scipy.stats as ss
import pandas as pd
import cv2
import matplotlib.pyplot as plt

from io import BytesIO
from urllib.parse import urlparse
from dotenv import load_dotenv

load_dotenv()

def azure_face(image_url):
  KEY = os.getenv("KEY")
  ENDPOINT = os.getenv("ENDPOINT")
  assert KEY
  assert ENDPOINT
  face_api_url = ENDPOINT +'/face/v1.0/detect'

  # image_url = 'https://upload.wikimedia.org/wikipedia/commons/3/37/Dagestani_man_and_woman.jpg'

  headers = {'Ocp-Apim-Subscription-Key': KEY}

  params = {
      'returnFaceId': 'false',
      'returnFaceLandmarks': 'true',
      'returnFaceAttributes': 'emotion,smile',
  }

  response = requests.post(face_api_url, params=params,
                          headers=headers, json={"url": image_url}).json()

  return response

def getRectangle(faceDictionary):
    rect = faceDictionary['faceRectangle']
    left = rect['left']
    top = rect['top']
    right = left + rect['width']
    bottom = top + rect['height']
    return ((left, top), (right, bottom))

def detection(url):
  # Detect a face in an image that contains a single face
  single_image_name = os.path.basename(url)
  print("Waiting for azure response")
  detected_faces = azure_face(url)
  if not detected_faces:
      raise Exception('No face detected from image {}'.format(single_image_name))
  if len(detected_faces) > 1:
      raise Exception('More than one face found in the image')
  # Download the image from the url
  print("Waiting for image download")
  response = requests.get(url)
  if response.status_code != 200:
    raise Exception('Internet error: status code {0}', response.status_code)
  img = cv2.imdecode(np.frombuffer(response.content, np.uint8), -1)
  return img, detected_faces

###Parameter: BGR formated image of the a pixel line from upperLipBottom to upperLipTop, and  threshold for teeth like color
###Return   : Boolean of whether the image contain teeth
###transform the image into HLS color space and use the threshold to determin whether there is teeth like color
def exist_teeth(bgr, lightness_threshold = 120, saturation_threshold = 30):
  hls = cv2.cvtColor(bgr, cv2.COLOR_BGR2HLS)
  ####apply threshold
  threshold = (hls[:,:,1]>lightness_threshold)&(hls[:,:,2]<saturation_threshold)
  rgb = bgr[:,:,[2,1,0]]
  rgb[np.invert(threshold)] = np.array([0,0,0])
  return np.any((hls[:,:,1]>lightness_threshold)&(hls[:,:,2]<saturation_threshold))

###Parameter: full image in BGR format, azure faceLandmarks attribute, threshold for teeth like color
###Return   : Boolean of whether the image contain teeth
###use faceLandmarks dict's upperLipBottom, underLipTop coordinator to extract the line image from upperLipBottom, underLipTop.
def teeth_detection(img, faceLandmarks, lightness_threshold = 120, saturation_threshold = 100):
  #bottom of the upper lip
  top = (int(faceLandmarks['upperLipBottom']['x']), int(faceLandmarks['upperLipBottom']['y']))
  #top of the lower lip
  bottom = (int(faceLandmarks['underLipTop']['x']), int(faceLandmarks['underLipTop']['y']))

  #mask: line between upper and lower lip
  mask = np.zeros(shape=img.shape, dtype = np.uint8)
  cv2.line(mask, top, bottom, (1,1,1), thickness=3)
  mask = mask.astype(bool)

  #apply the mask
  critical_area = img[mask]
  #reshape into image after masked
  critical_area = critical_area.reshape((int(critical_area.shape[0]/3),3))
  critical_area = np.array([critical_area])
  return exist_teeth(critical_area, lightness_threshold = lightness_threshold, saturation_threshold = saturation_threshold)

def face_analysis(url, mouth_open_threshold = 2, lightness_threshold = 120, saturation_threshold = 100):
  #load the image from internet and detection from azure
  #check whether only one faces is detected, otherwise throw exception
  img, detected_faces = detection(url)

  print('##########Analysis Start for {0}##########'.format(os.path.basename(url)))


  face_detection = detected_faces[0]

  #get the points of topleft, bottomright
  #[(topleft_x, topleft_y),(bottomright_x, bottomright_y)]
  faceloc = getRectangle(face_detection)

  ###ADD Blur###
  print("#########################################")
  print("Adding blur to blackground")
  #face area index
  img_face_slice = np.index_exp[faceloc[0][1]:faceloc[1][1], faceloc[0][0]:faceloc[1][0]]
  #make relica of the face
  face_img = np.copy(img[img_face_slice])
  #apply blur
  img = cv2.blur(img,(7,7))
  #recover the blured face from relica
  img[img_face_slice] = face_img

  nameError = True
  filename = os.path.basename(url)
  while(nameError):
      try:
          plt.imsave(filename+'.png', img[:,:,[2,1,0]])
          nameError = False
      except OSError:
          filename = input("File name error, the URL filename could not be use. Please enter a filename. If empty, the blur image will not be saved.\n")
          if(filename==""):
              nameError = False
  ##############

  ###IOU###
  print("#########################################")
  print("Checking IOU:")
  face_area = face_detection['faceRectangle']['width'] * face_detection['faceRectangle']['height']
  image_area = img.shape[0] * img.shape[1]
  iou = face_area/image_area
  if not(iou > 0.5):
    print('     face quality: face IOU({iou})  too small'.format(iou=iou))
  ##########

  ####teeth showing?#####
  print("#########################################")
  print("Checking teeth is showing:")
  faceLandmarks = face_detection['faceLandmarks']
  mouth_open = int(faceLandmarks['underLipTop']['y'])-int(faceLandmarks['upperLipBottom']['y'])<mouth_open_threshold
  if (mouth_open):
    print("     The mouth is close! no teeth is showing")
  else:
    if teeth_detection(np.copy(img), faceLandmarks, lightness_threshold = lightness_threshold, saturation_threshold = saturation_threshold):
      print("     teeth is shown")
    else:
      print("     teeth is not shown")

  #####sentiment#####
  print("#########################################")
  print("Checking sentiment:")
  for k, v in face_detection['faceAttributes']['emotion'].items():
    print("     {0}: {1}".format(k, v))

  print('######################Analysis End########################')

if __name__ == "__main__":
    face_analysis(sys.argv[1])
